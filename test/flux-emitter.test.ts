import Promise from 'ts-promise';
import { Thenable } from 'ts-promise';

const assert = require('assert');

import { FluxEmitter, Middleware } from '../src/flux-emitter';

function sleep(ms: number = 100) {
  return new Promise((done) => { setTimeout(done, ms) });
}

describe('FluxEmitter', () => {
  context('#update', () => {
    it('should be supported sync and async state update', async function (done) {
      const context = new FluxEmitter({ count: 0 });

      var _capture: any = null;
      context.on(':flux-apply', (s: any) => {
        _capture = s;
      });

      context.update((s: any) => ({ count: 1 }));
      assert.deepEqual(_capture, { count: 1 });

      const p1 = context.update(async function (s: any) {
        await sleep(100);
        return { count: 2 };
      });

      assert.deepEqual(_capture, { count: 1 }); 
      await p1;
      assert.deepEqual(_capture, { count: 2 });

      const p2 = context.update(async function (s: any) {
        await sleep(100);
        return { count: 3 };
      });

      const p3 = context.update(async function (s: any) {
        await sleep(100);
        return { count: 4 };
      });

      assert.equal( p2, p3 );

      await p2;

      assert.deepEqual( _capture, { count: 4 } );

      const p4 = context.update(async function (s: any) {
        await sleep(100);
        return  { count: 3 };
      });

      const p5 = context.update(async function (s: any) {
        await sleep(100);
        return  { count: 4 };
      });

      const p6 = context.update(async function (s: any) {
        await sleep(100);
        return  { count: 2 };
      })

      assert.equal( p4, p5 );
      assert.equal( p5, p6 );

      await p6;

      assert.deepEqual( _capture, { count: 2 } );

      done();
    });
  });
});
