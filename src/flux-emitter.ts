/// <reference path="../typings/main/ambient/node/index.d.ts" />

import Promise from 'ts-promise';
import { Thenable } from 'ts-promise';
import { EventEmitter } from 'events';

export interface Middleware {
  apply <T>(state: T|Thenable<T>): T|Thenable<T>;
}

export type Updater = <T>(state: T|Thenable<T>) => T|Thenable<T>;

export class FluxEmitter extends EventEmitter {
  public state: any;
  public middlewares: any;
  public updating: boolean;

  private updatingQueues: Array<Updater>;
  private updatingPromise: Promise<any>;

  constructor(initialState: any = {}) {
    super();

    this.state = initialState;
    this.middlewares = [];
    this.updating = false;
    this.updatingQueues = [];
    this.updatingPromise = null;

    this.subscribe();
  }

  public use(mw: Middleware): void {
    this.middlewares.push(mw);
  }

  private applyMiddlewares(state: any|Thenable<any>): any|Thenable<any> {
    return this.middlewares.reduce((s: any|Thenable<any>, mw: Middleware): any|Thenable<any> => {
      return mw.apply(s);
    }, state);
  }

  private finalize(nextState: any): Promise<void> {
    const inAsync = ( this.updatingPromise !== null );

    if ( inAsync ) {
      this.updatingQueues.length = 0;
      this.updatingPromise = null;
      this.updating = false;
    }

    this.state = nextState;

    this.emit(':flux-apply', this.state);

    if ( inAsync ) {
      this.emit(':end-async-updating');
    }

    return Promise.resolve();
  }

  public update(nextStateFn: Updater): Promise<any> {
    if ( this.updating ) {
      this.updatingQueues.push(nextStateFn);
      return this.updatingPromise;
    }

    const promiseOrState: any = this.applyMiddlewares(nextStateFn(this.state)); 

    if ( typeof(promiseOrState.then) !== 'function' ) {
      const oldState = this.state;

      this.finalize(promiseOrState);
      this.emit(':process-updating', this.state, oldState);

      return Promise.resolve();
    }

    this.updating = true
    this.emit(':start-async-updating');

    var endUpdate: Function; 
    this.updatingPromise = new Promise((done) => {
      endUpdate = done;
    });

    const lastState = this.state;
    promiseOrState.then((nextState: any): void|Thenable<any> => {
      this.emit(':process-async-updating', nextState, lastState);

      const updateLoop = (appliedState: any): any|Thenable<any> => {
        const nextFn = this.updatingQueues.shift();

        if (typeof(nextFn) !== 'function') {
          this.finalize(appliedState);
          endUpdate();
          return;
        }

        return Promise
          .resolve(this.applyMiddlewares(nextFn(appliedState)))
          .then((s: any|Thenable<any>): any|Thenable<any> => {
            this.emit(':process-async-updating', s, appliedState, this.updatingQueues.length);
            this.emit(':process-updating', s, appliedState);
            updateLoop(s);
          });
      };

      updateLoop(nextState);
    });

    return this.updatingPromise;
  }

  public subscribe(): void {
    // override this method on your class.
  }
}

