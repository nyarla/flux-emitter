FluxEmitter
===========

  * A [muzchi/flumpt](https://github.com/mizchi/flumpt) porting for general script with TypeScript.

Install
-------

```bash
npm install --save flux-emitter
```

Usage
-----

```typescript
import { FluxEmitter } from 'flux-emitter';
import Promise from 'ts-promise';

const assert = require('assert');

function sleep(ms: number = 100) {
  return new Promise((done) => { setTimeout(done, ms) });
}

const initialState  = { count: 0 };
const fluxEmitter   = new FluxEmitter(initialState);

var capture: any = null;

fluxEmitter.on(':flux-apply', (s: any) => {
  // your code is here,
  // for exmaple:
  capture = s;
});

(async function () {
  const p1 = fluxEmitter.update(async function (s) {
    await sleep(100);
    return { count: 3 };
  });

  await p1;

  assert.deepEqual( capture, { count: 3 } );
})();
```

Middlewares
-----------

```typescript
interface Middleware {
  apply <T>(state: T|Thenable<T>): T|Thenable<T>
}
```

Copyrights
----------

Original idea and implementation is written by [mizchi](https://github.com/mizchi),
and [mizchi/flumpt](https://github.com/mizchi/flumpt) is under the MIT-license.

Author
------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

License
-------

MIT
