Changelog
=========

## 1.0.0

  * renamed project from `caliburne` to `flux-emitter`
  * using TypeScript at now

## 0.2.0

  * supports for deku@2.0.0-rc5 (incompatible changes)

## 0.1.0

  * first release
